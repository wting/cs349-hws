1. Videos watched: bacon, pepper mill, apple pie, hammer

2.

- bacon
    - human salary / MSRP: $15 per hour / $2000 of bacon
    - machine replacement: racking, hanging, and moving the beef belly slabs,
      not too far in the future
    - automation difficulty: no

- pepper mill
    - human salary / MSRP: $15 per hour / $500 of pepper mills
    - machine replacement: pepper mill assembly, 10+ years
    - automation difficulty: QA is difficult to automate because it'd be
      testing all the way down.

3.

a. It's not currently cost effective to automate those tasks with a machine
at the moment.
b. There will always be some human element involved to oversee
quality control of a manufacturing process.
