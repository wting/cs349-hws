## What I Lived For

I challenged status quo, and always sought to better myself and those around me.

Early in my life, my wife and I discovered that we could not have children.
Starting a family had always been a dream, but biology does not always
cooperate.  However it was a mere setback, and through adoption, mentoring,
educational research, and teaching my wife and I have built our own family. Our
family may not be blood-related, but impact is much bigger than we could have
ever imagined and we wouldn't have it any other way.

Having worked a myriad of jobs and traveled the world early on, I became
intensely focused on building later on in life. Starting with simple products
and freelancing with a small client base, I moved on to larger clients and
eventually to creating a chain of failed or mildly successful businesses. I may
not have ever been wildly successful, but the euphoria gained from building and
delivering a great product cannot be replaced.

I was a builder, and will leave Earth knowing I have made it a better place.
