snowden: clean
	cp snowden.tex tmp.tex
	cp snowden.bib doc.bib
	latex tmp.tex
	bibtex tmp
	latex tmp.tex
	latex tmp.tex
	pdflatex tmp
	@-rm NSA.pdf
	mv tmp.pdf NSA.pdf
	rm tmp.*
	rm doc.*
	evince NSA.pdf

clean:
	@-rm tmp.*
	@-rm ./*.aux
	@-rm ./*.dvi
	@-rm ./*.log
	@-rm ./*.lof
	@-rm ./*.lot
	@-rm ./*.out
	@-rm ./*.toc
