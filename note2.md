I'm interested in Kant's definition of good as *good will*. From the book:

> A good will is good in and of itself. Even if a person's best efforts at doing
> good should fall short and cause harm, the good will behind the efforts is
> still good. Since a good will is the only thing that is universally good, the
> proper function of reason is to cultivate a will that is good in itself.

I believe this definition of good will and emphasis on the why behind actions is
insufficient as the basis for an ethical framework.

Let's consider the case where parents believe that prayer is the best action to
bring down a child's fever rather than taking him/her to the hospital. Based on
Kant's definition, this behavior is considered good will and should be
encouraged despite the negative outcome.
