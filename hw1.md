1.

    a. Google

    b. typing in `who is` results in the following suggested
    autocompletions:

        1. who is john galt
        2. who is gossip girl
        3. who is jodi arias

    c. I have no idea who any of these people are.

    Apparently John Galt is the hero from Ayn Rand's novel Atlas Shrugged. From
    the related "Who is John Galt?" [article][0]: "John Galt epitomizes all that is
    glorious of capitalism in its purist form — innovation, self-reliance, and
    freedom from government interference."

    Gossip Girl recently revealed some big secret in a TV series finale.

    Jodi Arias is the convicted defendant of a recent muder trial case.

    From this quick look into top results for a `who is` query, it would appear
    that people are interested in self-reliance, TV shows, and murder trial
    outcomes.

2.

    a. Google

    b. `are`:

        1. are mermaids real
        2. are we there yet
        3. are aliens real

    c. `is abortion`:

        1. is abortion legal
        2. is abortion legal in texas
        3. is abortion legal in mexico

    d. `is obama`:

        1. is obama gay
        2. is obama muslim
        3. is obama a good president

[0]: http://www.forbes.com/sites/robclarfeld/2012/02/15/who-is-john-galt/
